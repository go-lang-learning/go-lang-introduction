package main

import "fmt"

// Variable outside of main for a global value
var val = "global"

func main() {

	// a local variable
	val := 42

	/**
	* print the value of the local "val"
	* %T print typeof
	* %v print value
	 */
	fmt.Printf("%T, local val = %v\n", val, val)

	// Print value of the global
	printGlobal()

	// Update value of the global
	updateGlobal()
	printGlobal()

	// Update value of the local directly to the pointer address
	*(&val) = 100
	fmt.Printf("local updated val = %v\n", val)
}

func printGlobal() {
	fmt.Println("global val = ", val)
}

func updateGlobal() {
	val = "updated global"
}
