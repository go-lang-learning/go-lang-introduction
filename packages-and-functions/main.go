package main

// Import Go packages and Thrid-party packages
import (
	"fmt"

	proverbs "github.com/jboursiquot/go-proverbs"
)

func getRandomProverb() string {
	return proverbs.Random().Saying
}

func main() {
	fmt.Println(getRandomProverb())
}
