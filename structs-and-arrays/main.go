package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"
)

// define an author type
type author struct {
	name string
}

// define a book type
type book struct {
	title  string
	author author
}

// define a library type to track a list of books, index of author name
type library map[string][]book

// Add book to library
func (l library) addBook(b book) {
	l[b.author.name] = append(l[b.author.name], b)
}

// Lookup function to find books by author name
func (l library) findByAuthor(name string) []book {
	return l[name]
}

func main() {

	// create a new library
	lib := library{}

	// Add two books to the library by the same author
	rb := author{
		name: "Robert Martin",
	}

	lib.addBook(book{
		title:  "Clean Code",
		author: rb,
	})

	lib.addBook(book{
		title:  "Clean ArchitectureGo Tell It is on the Mountain",
		author: rb,
	})

	// Add one book to different author
	lib.addBook(book{
		title: "Refactoring",
		author: author{
			name: "Martin Fowler",
		},
	})

	// Dump the library with spew
	//spew.Dump(lib)

	// Search books by Author
	books := lib.findByAuthor(rb.name)

	// Not found Author
	jj := author{
		name: "Joseph Jr",
	}
	//books = lib.findByAuthor(jj.name)

	if len(books) > 0 {
		spew.Dump(books)
	} else {
		fmt.Println("None book find in the library with name", jj.name)
	}
}
